<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wordpress' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qhERlL[Ln%OyOszN57fyHdyirAYe0dWneASL;OtK{1SgvK.zBs4&Ia=JmCkTLW4X' );
define( 'SECURE_AUTH_KEY',  'i+3@*s{TYeZB9839K-xS~bDb0WyhNR(#HB;KN6{}UAk,<,~Y!{ ,f!|,7Lr/rwSH' );
define( 'LOGGED_IN_KEY',    'iSE!KMl}nBJJgQUA=qD^srQ5!Qp)G<:g3/NX1Y%y>qCkAX7(nnGR:?f&X1X_$x!+' );
define( 'NONCE_KEY',        'Eo4KP~r];hn`KF V`|)))IKTZb)q-QDY:l;`V]YT(HsD]VrqU9 n>|INW~&>0YE9' );
define( 'AUTH_SALT',        'OxK]KM.RZN8=^fODEI-A3pw!M KL54 z7d-UtKejff3Xf}qN9JE-Qqjr<`>]iCjQ' );
define( 'SECURE_AUTH_SALT', 'oHy^OZO)+a=%#R>r.~xy3peTeOZr&%tq`TyWL09A)yq#V2x5xojPpU8?|J1W}r!A' );
define( 'LOGGED_IN_SALT',   'njCp%~`n.uK@[J>HGuF&d,+}`rCab59%bezRP3J[1Czp,|vzv!z_*|R,oQV>+Jy*' );
define( 'NONCE_SALT',       'j/T;iT^y)dYV!#kQHrd+{;i-!10;{ aHPLZ(%i-QQ%v^]Vhw-bA]*]k*cR^;/Pj0' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
